from django.db import models

# Create your models here.
class Aluno(models.Model):
    id = models.IntegerField(primary_key=True)
    ra = models.IntegerField()
    nome = models.CharField(max_length=150)
    cpf = models.CharField(max_length=11)
    email = models.CharField(max_length=80)
    senha = models.CharField(max_length=30)

    def __str__(self):
        return self.nome , " - " , self.email

class Professor(models.Model):
    id = models.IntegerField(primary_key=True)
    nome = models.CharField(max_length=150)
    cpf = models.CharField(max_length=11)
    email = models.CharField(max_length=80)
    senha = models.CharField(max_length=30)

    def __str__(self):
        return self.nome , " - " , self.email

class Coordenador(models.Model):
    id = models.IntegerField(primary_key=True)
    nome = models.CharField(max_length=150)
    cpf = models.CharField(max_length=11)
    email = models.CharField(max_length=80)
    senha = models.CharField(max_length=30)

    def __str__(self):
        return self.nome , " - " , self.email
    