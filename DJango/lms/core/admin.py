from django.contrib import admin
from .models import Aluno,Professor,Coordenador

# Register your models here.
admin.site.register(Aluno)
admin.site.register(Professor)
admin.site.register(Coordenador)